package net.camtech.verification;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class CamVerify extends JavaPlugin{

    Logger logger;
    private SocketServer socketServer = new SocketServer();
    private Thread thread;
    
    @Override
    public void onEnable()
    {
        this.logger = Logger.getLogger("Minecraft");
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.log(Level.INFO, "{0}{1} has been enabled!", new Object[]{pdfFile.getName(), pdfFile.getVersion()});
        if(pdfFile.getVersion().toLowerCase().contains("alpha"))
        {
            this.logger.log(Level.WARNING, "{0} IS IN A VERY UN-TESTED ALPHA, DO NOT BE SURPRISED IF SOMETHING GOES WRONG!", pdfFile.getName());
        }
        if(pdfFile.getVersion().toLowerCase().contains("beta"))
        {
            this.logger.warning("This is a beta build, it should be fairly stable but there may be a few errors, please report them on the bug tracker!");
        }
        thread = new Thread(socketServer);
        thread.start();
    }
    
    @Override
    public void onDisable()
    {
        PluginDescriptionFile pdfFile = this.getDescription();
        this.logger.log(Level.INFO, "{0} has been disabled! ~ Goodbye!", pdfFile.getName());
        try
        {
            this.socketServer.sock.close();
        } catch(IOException ex)
        {
            this.logger.severe(ex.getMessage());
        }
    }
    
}
